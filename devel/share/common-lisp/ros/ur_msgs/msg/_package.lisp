(cl:defpackage ur_msgs-msg
  (:use )
  (:export
   "<ANALOG>"
   "ANALOG"
   "<DIGITAL>"
   "DIGITAL"
   "<IOSTATES>"
   "IOSTATES"
   "<TOOLDATAMSG>"
   "TOOLDATAMSG"
   "<ROBOTSTATERTMSG>"
   "ROBOTSTATERTMSG"
   "<MASTERBOARDDATAMSG>"
   "MASTERBOARDDATAMSG"
  ))

